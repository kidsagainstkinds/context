﻿namespace Context
{
    public interface IContextInitializable
    {
        void InitializeByContext(IContext context);
    }
}
