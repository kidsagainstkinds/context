﻿using Context.Components.Base;

namespace Context.Components.Default
{
	internal class DefaultInstanceBuilder : ContextComponentBase, IInstanceBuilder
	{
		public void BuildUp(object target)
		{
			Context.BuildUp(target);
		}
	}
}