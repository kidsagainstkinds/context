﻿using Context.Components.Base;

namespace Context.Components.Default
{
	internal class DefaultControlHandle: ContextComponentBase, IControlHandle
	{
		public void ReleaseContext()
		{
			Context.Release();
		}

		public void CleanUpContext()
		{
			Context.CleanUpInstances();
		}
	}
}
