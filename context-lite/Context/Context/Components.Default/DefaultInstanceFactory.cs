﻿using Context.Components.Base;

namespace Context.Components.Default
{
	internal class DefaultInstanceFactory : ContextComponentBase, IInstanceFactory
	{
		public TInstance CreateInstance<TInstance>(object tag = null) where TInstance : class
		{
			return Context.Resolve<TInstance>(tag);
		}
	}
}