﻿namespace Context.Components.Base
{
	public abstract class ResolverProviderComponentBase : ContextComponentBase, IResolverProviderComponent
	{
		public abstract void Register();
	}
}