﻿using System;
using Context.Base;

namespace Context.Components.Base
{
	public abstract class ContextComponentBase : IContextComponent
	{
		private readonly WeakReference<ComponentContextBase> _contextReference = new WeakReference<ComponentContextBase>(null);

		protected ComponentContextBase Context =>
			_contextReference.TryGetTarget(out var context)
			? context
			: throw new Exception($"[{GetType().Name}] this component lost reference on appropriate context");

		public bool ContextIsValid => _contextReference.TryGetTarget(out var _);

		internal void Initialize(ComponentContextBase context)
		{
			_contextReference.SetTarget(context);
		}
	}
}