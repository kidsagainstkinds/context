﻿using System.Collections.Generic;
using Context.Components.Default;

namespace Context.Components.Base
{
	public abstract class ContextComponentsBase : IContextComponents
	{
		public virtual IControlHandle Handle { get; } = new DefaultControlHandle();
		public virtual IInstanceFactory Factory { get; } = new DefaultInstanceFactory();
		public virtual IInstanceBuilder Builder { get; } = new DefaultInstanceBuilder();

		protected virtual IResolverProviderComponent[] ResolverProvidersOnly { get; } = new IResolverProviderComponent[0]; 

		public virtual IEnumerable<IContextComponent> GetComponents()
		{
			yield return Handle;
			yield return Factory;
			yield return Builder;

			foreach (var component in ResolverProvidersOnly)
			{
				yield return component;
			}
		}
	}
}