﻿using System;
using System.Linq;
using System.Reflection;
using Context._Impl;
using Context.Attributes;

namespace Context.Base
{
	public abstract class InjectionContextBase : ContextBase
	{
		protected override void PreInitializeInstance(object instance)
		{
			foreach(var member in instance.GetType()
				.GetMembers(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public |
							BindingFlags.SetField | BindingFlags.SetProperty | BindingFlags.FlattenHierarchy)
				.Where(_ => _.GetCustomAttributes(typeof(InjectAttribute), true).Any()))
			{
				if(!(TrySetField(member as FieldInfo, instance) || TrySetProperty(member as PropertyInfo, instance)))
				{
					throw new Exception($"Can't be done injection to member of type: {member.GetType()}");
				}
			}
		}

		private bool TrySetField(FieldInfo field, object instance)
		{
			if(field == null)
			{
				return false;
			}

			field.SetValue(instance,
				Resolve(new TypeUid(field.FieldType, null,
					field.GetCustomAttributes(typeof(InjectAttribute), true).Cast<InjectAttribute>().First().Tag)));

			return true;
		}

		private bool TrySetProperty(PropertyInfo property, object instance)
		{
			if(property == null)
			{
				return false;
			}

			property.SetValue(instance,
				Resolve(new TypeUid(property.PropertyType, null,
					property.GetCustomAttributes(typeof(InjectAttribute), true).Cast<InjectAttribute>().First().Tag)),
				null);

			return true;
		}
	}
}