﻿using System.Linq;
using Context.Components;
using Context.Components.Base;

namespace Context.Base
{
	public abstract class ComponentContextBase : InjectionContextBase
	{
	}

	public abstract class ComponentContextBase<TContextComponents> : ComponentContextBase
		where TContextComponents : ContextComponentsBase, new()
	{
		protected TContextComponents Components { get; private set; }

		protected override void OnRegisterContext()
		{
			Components = new TContextComponents();

			PreInitializeComponents();

			foreach (var component in Components.GetComponents().OfType<ContextComponentBase>())
			{
				component.Initialize(this);
			}

			RegisterInstance<IContextComponents>(Components);

			foreach (var component in Components.GetComponents().OfType<IResolverProviderComponent>())
			{
				component.Register();
			}
		}

		protected virtual void PreInitializeComponents(){ }
	}
}