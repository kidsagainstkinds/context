﻿using System;

namespace Context._Impl
{
	public partial class ContextBase : IContext
    {
        public T Resolve<T>(object tag = null) where T : class
        {
            if(_resolvers.TryGetValue(new TypeUid(typeof(T), null, tag), out var resolver))
            {
                var initialize = (resolver as IInstanceResolver)?.Instance == null;
                var instance = resolver.Resolve<T>();
                if(initialize)
                {
                    PreInitializeInstance(instance);
                    (instance as IContextInitializable)?.InitializeByContext(this);
                }
                return instance;
            }

            throw new Exception($"Can't find resolver by {new TypeUid(typeof(T), null, tag)}");
        }

        public T Resolve<T, TArg1>(TArg1 arg1, object tag = null) where T : class
            where TArg1 : class
        {
            if(_resolvers.TryGetValue(new TypeUid(typeof(T), typeof(Arg<TArg1>), tag), out var resolver))
            {
                var instance = resolver.Resolve<T, TArg1>(arg1);
                PreInitializeInstance(instance);
                (instance as IContextInitializable)?.InitializeByContext(this);
                return instance;
            }

            throw new Exception($"Can't find resolver by {new TypeUid(typeof(T), typeof(Arg<TArg1>), tag)}");
        }

        public T Resolve<T, TArg1, TArg2>(TArg1 arg1, TArg2 arg2, object tag = null) where T : class
            where TArg1 : class
            where TArg2 : class
        {
            if(_resolvers.TryGetValue(new TypeUid(typeof(T), typeof(Arg<TArg1, TArg2>), tag), out var resolver))
            {
                var instance = resolver.Resolve<T, TArg1, TArg2>(arg1, arg2);
                PreInitializeInstance(instance);
                (instance as IContextInitializable)?.InitializeByContext(this);
                return instance;
            }

            throw new Exception($"Can't find resolver by {new TypeUid(typeof(T), typeof(Arg<TArg1, TArg2>), tag)}");
        }

        public T Resolve<T, TArg1, TArg2, TArg3>(TArg1 arg1, TArg2 arg2, TArg3 arg3, object tag = null) where T : class
            where TArg1 : class
            where TArg2 : class
            where TArg3 : class
        {
            if(_resolvers.TryGetValue(new TypeUid(typeof(T), typeof(Arg<TArg1, TArg2, TArg3>), tag), out var resolver))
            {
                var instance = resolver.Resolve<T, TArg1, TArg2, TArg3>(arg1, arg2, arg3);
                PreInitializeInstance(instance);
                (instance as IContextInitializable)?.InitializeByContext(this);
                return instance;
            }

            throw new Exception($"Can't find resolver by {new TypeUid(typeof(T), typeof(Arg<TArg1, TArg2, TArg3>), tag)}");
        }

        public T TryResolve<T>(object tag = null) where T : class
        {
            if(_resolvers.TryGetValue(new TypeUid(typeof(T), null, tag), out var resolver))
            {
                var initialize = (resolver as IInstanceResolver)?.Instance == null;
                var instance = resolver.Resolve<T>();
                if(initialize)
                {
                    PreInitializeInstance(instance);
                    (instance as IContextInitializable)?.InitializeByContext(this);
                }
                return instance;
            }

            return null;
        }

	    protected object Resolve(TypeUid typeUid)
        {
            if(_resolvers.TryGetValue(typeUid, out var resolver))
            {
                var initialize = (resolver as IInstanceResolver)?.Instance == null;
                var instance = resolver.Resolve();
                if(initialize)
                {
                    PreInitializeInstance(instance);
                    (instance as IContextInitializable)?.InitializeByContext(this);
                }
                return instance;
            }

            throw new Exception($"Can't find resolver by {typeUid}");
        }

        public T GetSafeWithoutResolve<T>(object tag = null) where T : class
        {
            return _resolvers.TryGetValue(new TypeUid(typeof(T), null, tag), out var resolver)
                ? (resolver as IInstanceResolver)?.Instance as T
                : null;
        }
    }
}
