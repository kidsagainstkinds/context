﻿using System.Collections.Generic;
using System.Linq;

namespace Context._Impl
{
	public abstract partial class ContextBase
	{
		private readonly Dictionary<TypeUid, IResolver> _resolvers = new Dictionary<TypeUid, IResolver>();

		private ContextBase _previousContext;
		private List<IResolver> _currentResolvers;

		protected virtual bool ShouldOverridePreviousResolvers()
		{
			return true;
		}

		public void RegisterContext(IContext previousContext)
		{
			_previousContext = previousContext as ContextBase;

			OnRegisterContext();

			_currentResolvers = _resolvers.Values.ToList();

			LoadContextFromPrevious(_previousContext?._resolvers);

			foreach(var initializable in _currentResolvers.OfType<InstanceResolver>().Select(_ => _.Instance).Distinct())
			{
				PreInitializeInstance(initializable);
				(initializable as IContextInitializable)?.InitializeByContext(this);
			}
		}

		public void BuildUp(object instance)
		{
			PreInitializeInstance(instance);
			(instance as IContextInitializable)?.InitializeByContext(this);
		}

		public void Release()
		{
			foreach(var releasable in _currentResolvers.OfType<IInstanceResolver>()
				.Select(_ => _.Instance)
				.Where(_ => _ != null)
				.Distinct()
				.OfType<IContextReleasable>())
			{
				releasable.ReleaseByContext(this);
			}

			OnContextReleased();
		}

		public void CleanUpInstances()
		{
			foreach(var cleanable in _currentResolvers.OfType<IInstanceResolver>()
				.Select(_ => _.Instance)
				.Where(_ => _ != null)
				.Distinct()
				.OfType<IContextCleanable>())
			{
				cleanable.CleanUpByContext(this);
			}
		}

		protected abstract void OnRegisterContext();

		protected virtual void OnContextReleased() { }

		protected virtual void PreInitializeInstance(object instance) { }

		private void LoadContextFromPrevious(Dictionary<TypeUid, IResolver> previousResolvers)
		{
			if(previousResolvers == null)
			{
				return;
			}

			foreach(var pair in previousResolvers)
			{
				if(!_resolvers.ContainsKey(pair.Key) || !ShouldOverridePreviousResolvers())
				{
					_resolvers[pair.Key] = pair.Value;
				}
			}
		}
	}
}