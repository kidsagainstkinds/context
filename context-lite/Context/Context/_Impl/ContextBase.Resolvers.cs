﻿using System;

namespace Context._Impl
{
	public partial class ContextBase
    {
        protected interface IResolver
        {
            T Resolve<T>() where T : class;

            T Resolve<T, TArg1>(TArg1 arg1) where T : class
                where TArg1 : class;

            T Resolve<T, TArg1, TArg2>(TArg1 arg1, TArg2 arg2) where T : class
                where TArg1 : class
                where TArg2 : class;

            T Resolve<T, TArg1, TArg2, TArg3>(TArg1 arg1, TArg2 arg2, TArg3 arg3) where T : class
                where TArg1 : class
                where TArg2 : class
                where TArg3 : class;

            object Resolve();
        }

        private abstract class ResolverBase : IResolver
        {
            public virtual T Resolve<T>() where T : class
            {
                throw new Exception($"{GetType().GetGenericTypeDefinition()?.Name} resolver doesn't handle this method");
            }

            public virtual T Resolve<T, TArg1>(TArg1 arg1) where T : class
                where TArg1 : class
            {
                throw new Exception($"{GetType().GetGenericTypeDefinition()?.Name} resolver doesn't handle this method");
            }

            public virtual T Resolve<T, TArg1, TArg2>(TArg1 arg1, TArg2 arg2) where T : class
                where TArg1 : class
                where TArg2 : class
            {
                throw new Exception($"{GetType().GetGenericTypeDefinition()?.Name} resolver doesn't handle this method");
            }

            public virtual T Resolve<T, TArg1, TArg2, TArg3>(TArg1 arg1, TArg2 arg2, TArg3 arg3) where T : class
                where TArg1 : class
                where TArg2 : class
                where TArg3 : class
            {
                throw new Exception($"{GetType().GetGenericTypeDefinition()?.Name} resolver doesn't handle this method");
            }

            public virtual object Resolve()
            {
                throw new Exception($"{GetType().GetGenericTypeDefinition()?.Name} resolver doesn't handle this method");
            }
        }

        private interface IInstanceResolver
        {
            object Instance { get; }
            T Resolve<T>() where T : class;
            object Resolve();
        }

        private class InstanceResolver : ResolverBase, IInstanceResolver
        {
            public object Instance { get; }

            public InstanceResolver(object instance)
            {
                Instance = instance;
            }

            public override T Resolve<T>() => Instance as T;

            public override object Resolve() => Instance;
        }

        private class LazyInstanceResolver<TResolve> : ResolverBase, IInstanceResolver
            where TResolve : class
        {
            private readonly Func<TResolve> _ctor;
            private object _instance;

            public object Instance => _instance;

            public LazyInstanceResolver(Func<TResolve> ctor)
            {
                _ctor = ctor;
            }

            public override T Resolve<T>() => (_instance ?? (_instance = _ctor())) as T;

            public override object Resolve() => _instance ?? (_instance = _ctor());
        }

        private class TypeResolver<TResolve> : ResolverBase where TResolve : new()
        {
            public override T Resolve<T>() => new TResolve() as T;

            public override object Resolve() => new TResolve();
        }

        private class TypeResolver0<TResolve> : ResolverBase where TResolve : class
        {
            private readonly Func<TResolve> _ctor;

            public TypeResolver0(Func<TResolve> ctor)
            {
                _ctor = ctor;
            }

            public override T Resolve<T>() => _ctor() as T;

            public override object Resolve() => _ctor();
        }

        private class TypeResolver1<TResolve, TCtorArg1> : ResolverBase where TResolve : class
            where TCtorArg1 : class
        {
            private readonly Func<TCtorArg1, TResolve> _ctor;

            public TypeResolver1(Func<TCtorArg1, TResolve> ctor)
            {
                _ctor = ctor;
            }

            public override T Resolve<T, TArg1>(TArg1 arg1) => _ctor(arg1 as TCtorArg1) as T;
        }

        private class TypeResolver2<TResolve, TCtorArg1, TCtorArg2> : ResolverBase where TResolve : class
            where TCtorArg1 : class
            where TCtorArg2 : class
        {
            private readonly Func<TCtorArg1, TCtorArg2, TResolve> _ctor;

            public TypeResolver2(Func<TCtorArg1, TCtorArg2, TResolve> ctor)
            {
                _ctor = ctor;
            }

            public override T Resolve<T, TArg1, TArg2>(TArg1 arg1, TArg2 arg2)
                => _ctor(arg1 as TCtorArg1, arg2 as TCtorArg2) as T;
        }

        private class TypeResolver3<TResolve, TCtorArg1, TCtorArg2, TCtorArg3> : ResolverBase where TResolve : class
            where TCtorArg1 : class
            where TCtorArg2 : class
            where TCtorArg3 : class
        {
            private readonly Func<TCtorArg1, TCtorArg2, TCtorArg3, TResolve> _ctor;

            public TypeResolver3(Func<TCtorArg1, TCtorArg2, TCtorArg3, TResolve> ctor)
            {
                _ctor = ctor;
            }

            public override T Resolve<T, TArg1, TArg2, TArg3>(TArg1 arg1, TArg2 arg2, TArg3 arg3) =>
                _ctor(arg1 as TCtorArg1, arg2 as TCtorArg2, arg3 as TCtorArg3) as T;
        }

    }
}
