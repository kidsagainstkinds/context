﻿using System;

// ReSharper disable UnusedTypeParameter
namespace Context._Impl
{
	public partial class ContextBase
	{
		private class Arg<TArg1>
			where TArg1 : class
		{
		}

		private class Arg<TArg1, TArg2>
			where TArg1 : class
			where TArg2 : class
		{
		}

		private class Arg<TArg1, TArg2, TArg3>
			where TArg1 : class
			where TArg2 : class
			where TArg3 : class
		{
		}

		protected struct TypeUid
		{
			private readonly Type _type;
			private readonly Type _args;
			private readonly object _tag;

			public TypeUid(Type type, Type args, object tag)
			{
				_type = type;
				_args = args;
				_tag = tag;
			}

			public override string ToString()
			{
				return $"{_type.Name} args: {_args?.GetGenericTypeDefinition()} with tag {_tag}";
			}
		}
	}
}
