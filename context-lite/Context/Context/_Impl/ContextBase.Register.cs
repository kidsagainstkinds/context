﻿using System;

namespace Context._Impl
{
	public abstract partial class ContextBase
    {
        public void RegisterInstance<TKey>(object instance, object tag = null)
        {
            _resolvers[new TypeUid(typeof(TKey), null, tag)] = new InstanceResolver(instance);
        }

	    public void RegisterInstance<TKey, TResolve>(Func<TResolve> ctor, object tag = null) where TResolve : class
        {
            _resolvers[new TypeUid(typeof(TKey), null, tag)] = new LazyInstanceResolver<TResolve>(ctor);
        }

	    public void RegisterType<TKey, TResolve>(object tag = null) where TResolve : class, new()
        {
            _resolvers[new TypeUid(typeof(TKey), null, tag)] = new TypeResolver<TResolve>();
        }

	    public void RegisterType<TResolve>(Func<TResolve> ctor, object tag = null) where TResolve : class
        {
            _resolvers[new TypeUid(typeof(TResolve), null, tag)] = new TypeResolver0<TResolve>(ctor);
        }

        public void RegisterType<TResolve, TArg1>(Func<TArg1, TResolve> ctor, object tag = null)
            where TResolve : class
            where TArg1 : class
        {
            _resolvers[new TypeUid(typeof(TResolve), typeof(Arg<TArg1>), tag)] = new TypeResolver1<TResolve, TArg1>(ctor);
        }

	    public void RegisterType<TResolve, TArg1, TArg2>(Func<TArg1, TArg2, TResolve> ctor, object tag = null)
           where TResolve : class
           where TArg1 : class
           where TArg2 : class
        {
            _resolvers[new TypeUid(typeof(TResolve), typeof(Arg<TArg1, TArg2>), tag)] = new TypeResolver2<TResolve, TArg1, TArg2>(ctor);
        }

	    public void RegisterType<TResolve, TArg1, TArg2, TArg3>(Func<TArg1, TArg2, TArg3, TResolve> ctor, object tag = null)
           where TResolve : class
           where TArg1 : class
           where TArg2 : class
           where TArg3 : class
        {
            _resolvers[new TypeUid(typeof(TResolve), typeof(Arg<TArg1, TArg2, TArg3>), tag)] = new TypeResolver3<TResolve, TArg1, TArg2, TArg3>(ctor);
        }

	    public bool ContainsRegistered<TKey>(object tag = null) => _resolvers.ContainsKey(new TypeUid(typeof(TKey), null, tag));
    }
}
