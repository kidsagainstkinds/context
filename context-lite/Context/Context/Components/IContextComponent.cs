﻿namespace Context.Components
{
	public interface IContextComponent
	{
		bool ContextIsValid { get; }
	}
}
