﻿namespace Context.Components
{
	public interface IControlHandle : IContextComponent
	{
		void ReleaseContext();
		void CleanUpContext();
	}
}