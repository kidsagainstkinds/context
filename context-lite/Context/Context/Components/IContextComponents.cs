﻿using System.Collections.Generic;

namespace Context.Components
{
	public interface IContextComponents
	{
		IControlHandle Handle { get; }
		IInstanceFactory Factory { get; }
		IInstanceBuilder Builder { get; }

		IEnumerable<IContextComponent> GetComponents();
	}
}