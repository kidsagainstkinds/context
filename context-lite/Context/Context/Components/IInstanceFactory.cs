﻿namespace Context.Components
{
	public interface IInstanceFactory : IContextComponent
	{
		TInstance CreateInstance<TInstance>(object tag = null) where TInstance : class;
	}
}