﻿namespace Context.Components
{
	public interface IResolverProviderComponent: IContextComponent
	{
		void Register();
	}
}