﻿namespace Context.Components
{
	public interface IInstanceBuilder : IContextComponent
	{
		void BuildUp(object target);
	}
}