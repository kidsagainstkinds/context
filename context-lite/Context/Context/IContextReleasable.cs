﻿namespace Context
{
    public interface IContextReleasable
    {
        void ReleaseByContext(IContext context);
    }
}