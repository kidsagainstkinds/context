﻿namespace Context
{
	public interface IContextCleanable
	{
		void CleanUpByContext(IContext context);
	}
}