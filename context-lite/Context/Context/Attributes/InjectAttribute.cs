﻿using System;

namespace Context.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class InjectAttribute : Attribute
    {
        public object Tag { get; }

        public InjectAttribute(object tag = null)
        {
            Tag = tag;
        }
    }
}
