﻿namespace Context
{
    public interface IContext
    {
        T Resolve<T>(object tag = null) where T : class;

        T Resolve<T, TArg1>(TArg1 arg1, object tag = null) where T : class
            where TArg1 : class;

        T Resolve<T, TArg1, TArg2>(TArg1 arg1, TArg2 arg2, object tag = null) where T : class
            where TArg1 : class
            where TArg2 : class;

        T Resolve<T, TArg1, TArg2, TArg3>(TArg1 arg1, TArg2 arg2, TArg3 arg3, object tag = null) where T : class
            where TArg1 : class
            where TArg2 : class
            where TArg3 : class;

        T TryResolve<T>(object tag = null) where T : class;
    }
}